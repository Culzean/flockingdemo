
#pragma once

#include <stdio.h>
#include <iostream>
#include <ctime>

#include <math.h>

#include <glew.h>
#include <SDL.h>
#include <glm.hpp>
#include <gtc\type_ptr.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtx\random.hpp>
#include <gtx\ocl_type.hpp>
#include <sstream>
#include <CL\opencl.h>

#include "AGPMatrixStack.h"
#include "TextureManager.h"
#include "Renderable.h"
#include "AGP_Camera.h"
#include "AGPShader.h"
#include "Flock.h"
#include "Plane.h"

#define SCREEN_WIDTH	1200
#define	SCREEN_HEIGHT	800

//easier to use define sometimes
#define PI				3.141592654f
#define HALF_PI			3.141592654f / 2

#define GRAVITY			9.806650f
#define SIGN(x)			(((x) < 0) ? -1 : 1)