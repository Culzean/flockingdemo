/*
Class to deal with the flock
much of this is thanks to Woodcock from game programming gems 1 source
but reworked with my opencl handler set up
*/

#ifndef FLOCK_H
#define FLOCK_H

#include "stafx.h"
#include "OpenCLHandler.h"

class OpenCLHandler;
class AGPShader;

struct Boid {
	Boid(){vPos = glm::gtx::cl_float4(0.0f);};
	Boid(glm::vec4& _pos, glm::vec4& _vel, glm::vec4& _accel)
		: vPos(_pos), vVel(_vel), vAccel(_accel){}
	glm::gtx::cl_float4 vPos, vVel, vAccel;
	glm::gtx::cl_int	iBoidSeen;
} CL_ALIGNED(32);

//sent range of inputs
const static GLint				MIN_BOID_VIEW		= 2;
const static GLint				MAX_BOID_VIEW		= 22;
const static GLfloat			MIN_VIEW_DIST		= 0.03;
const static GLfloat			MAX_VIEW_DIST		= 30.0f;
const static GLfloat			MIN_FLOCK_COEH		= 10.0f;
const static GLfloat			MAX_FLOCK_COEH		= 0.2f;
const static GLfloat			MIN_ALIGN			= 0.0f;
const static GLfloat			MAX_ALIGN			= 1.0f;
const static GLint				NO_INCR				= 8;


class Flock : public Renderable{

public:
	Flock(GLint _width, GLint _depth);
	virtual ~Flock();

	virtual void Draw(AGPShader* shader);
	virtual void Init();
	virtual void Init(GLuint _width, GLuint _depth);
	//void Init(glm::vec3 _pos, bool drawDebug);

	static void DrawDebug(glm::vec4 _vDraw, glm::vec4 _vStart);

	virtual void Update();

	bool AddBoid( Boid _newBoid );

	void setMaxSpeed(GLfloat _max)		{	this->clMaxSpeed = _max;	};
	GLfloat getMaxSpeed()				{	return this->clMaxSpeed;	}

	void Bind();

	void SetInput( GLint value, GLint incr );

private:

	GLfloat			averageSpeed;

	OpenCLHandler*	clHandler;

	static void DrawDebug();

	bool BindCLBuffers();
	void BindKernel( );
	void BindGLBuffers();

	std::vector< Boid* >			flock;
	GLuint			iNoBoids;

	void DefineInputs();
	glm::gtx::cl_float4			vUserInput;
	glm::vec4					vUserSteps;
	glm::vec4					vUserMax;
	glm::vec4					vUserMin;

	//glm or not?
	glm::gtx::cl_float4*		vVel;
	glm::gtx::cl_float4*		vAccel;
	glm::gtx::cl_float4*		vPos;
	glm::gtx::cl_float3			clBounds;
	cl_mem						clFlockIn;
	cl_mem						clFlockOut;
	cl_float					clMaxSpeed;

	cl_mem			clRand;
	cl_mem			clVel;
	cl_mem			clAccel;
	cl_mem			clPos;
};


#endif