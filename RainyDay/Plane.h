#ifndef PLANE_H
	#define PLANE_H

#include "Mesh.h"
#include "stafx.h"

class Mesh;

class Plane
{
private:
	glm::vec3 normal;
	glm::vec3 pos0;
	glm::vec3 pos1;
	glm::vec3 pos2;
	glm::vec3 pos3;
	glm::vec3 centre;
	GLfloat width, depth;

	GLfloat		planeDot;
	Mesh*		pMesh;

public:
	Plane::Plane(glm::vec3 pos0, glm::vec3 pos1, glm::vec3 pos2, glm::vec3 pos3);
	~Plane();

	void Init(GLuint iNoPointsWidth, GLuint iNoPointDepth);
	void PrintOut();

	Mesh*		getMesh()			{	return this->pMesh;	}

	glm::vec3 getNormal()			{	return normal;	}
	glm::vec3 getPos()				{	return centre;	}
	GLfloat		getWidth()			{	return width;	}
	GLfloat		getDepth()			{	return depth;	}

	GLfloat		findPoint( GLfloat x, GLfloat z );

	glm::vec3 FindNormal();
};

#endif