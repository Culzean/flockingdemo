#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H


#include <map>
#include <string>

#include <iostream>
#include <assert.h>
#include <SDL_image.h>
#include "stafx.h"



class TextureManager {
private:
	static TextureManager*		pTexManager;
	std::map<std::string, GLuint> Texmap;
	std::map<std::string, GLuint>::iterator index;
	static int lastID;
public:
	//method to get singleton instance
	static TextureManager* GetInstance(){
	if(!pTexManager)
		pTexManager = new TextureManager();
	return pTexManager;
	
	}
	static void DestroyInstance(){
		if(pTexManager)
			delete pTexManager;	pTexManager = NULL;

	}
	~TextureManager();
	TextureManager();
	void InitTextures(void);
	GLuint LoadTexBMP(char *fname);
	GLuint LoadTexPNG( const char* fname);
	GLuint loadTexture(const char* fname);
	GLuint LoadCubeMapTexPNG(const char *fname[]);
	GLuint LoadCubeMapTexBMP(const char *fname[]);
	GLuint getTexID(const char* fname);
};

#endif