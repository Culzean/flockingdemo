////////////////////////////////////////////////
//
//Game class included from RT3D project and retro fitted for SDL
//by B00226804

#ifndef GAMECLASS_H
	#define GAMECLASS_H

#include <SDL.h>

#include "Renderable.h"
#include "GameData.h"
#include "stafx.h"
#include <stdio.h>
#include <stdlib.h>

class GameClass
{
private:

	GameData*			model;
	TextureManager*		texManager;
	AGPMatrixStack*		matrixStack;

	glm::mat4 projection;
	AGPMatrixStack*		MVP;

	SDL_GLContext glContext; //OpenGl context handle
	SDL_Window *hWindow; //window handle
	SDL_Event sdlEvent;

	//OpenCLHandler* clHandler;

	bool running;

	float dt;
	clock_t ticks;
	clock_t beginTime;
	clock_t nextFrame;
	clock_t frameTime;
	int		frames;

	float				fps;
	bool				m_bComplete;

	glm::ivec2	mousePos;
	glm::vec3 input;

	GLuint			iUserVar;

public:
	GameClass() {};
	~GameClass() {};
	static GameClass* pGame;

	static GameClass* GetInstance(){
	if(! pGame)
		pGame = new GameClass;
	return pGame;
	};
	static void DestroyInstance(){
		if(pGame)
		{delete pGame; pGame = NULL;}}

	void CleanUp();

	bool handleSDLEvent(SDL_Event const &sdlEvent);
	void MainLoop(void);
	void SetupWindow(int argc, char* argv[]);
	bool Init();
	void Update();
	void Render(SDL_Window* window);
};


#endif