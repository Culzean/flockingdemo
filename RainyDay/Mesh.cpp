#include "Mesh.h"


Mesh::Mesh() {

}

Mesh::~Mesh() {
	glDeleteBuffers(5, vbo);
	glDeleteVertexArrays(1, &vao);

	delete [] vbo;
}

void Mesh::Init() {
}

void Mesh::createVertices(GLfloat iWidth, GLfloat iDepth, GLint iMaxWidth, GLint iMaxDepth) {

	GLint iNoPointsWidth = (GLint)glm::ceil(iWidth / (GLfloat)iMaxWidth);
	GLint iNoPointDepth = (GLint)glm::ceil(iDepth / (GLfloat)iMaxDepth);

	BeginMesh(iNoPointsWidth* iNoPointDepth * 6);

	glm::vec3 vVerts[4];
	glm::vec3 vNorms[4];
	glm::vec2 vTexCoords[4];


	for(int iX =0; iX< iNoPointsWidth; ++iX)
	{
		GLfloat posX = iX * (GLfloat)iMaxWidth;
		for(int iZ =0; iZ< iNoPointDepth; ++iZ)
		{
			GLfloat posZ = iZ * (GLfloat)iMaxDepth;

			vVerts[0][0] = posX;
			vVerts[0][1] = 0.0f;
			vVerts[0][2] = posZ;

			vVerts[1][0] = posX + iMaxWidth;
			vVerts[1][1] = 0.0f;
			vVerts[1][2] = posZ;

			vVerts[2][0] = posX + iMaxWidth;
			vVerts[2][1] = 0.0f;
			vVerts[2][2] = posZ + iMaxDepth;

			vVerts[3][0] = posX;
			vVerts[3][1] = 0.0f;
			vVerts[3][2] = posZ + iMaxDepth;

			vTexCoords[0].s = 0.0f;
			vTexCoords[0].t = 1.0f;

			vTexCoords[1].s = 1.0f;
			vTexCoords[1].t = 1.0f;

			vTexCoords[2].s = 1.0f;
			vTexCoords[2].t = 0.0f;

			vTexCoords[3].s = 1.0f;
			vTexCoords[3].t = 0.0f;

			vNorms[0][0] = 0;		vNorms[0][1] = 1;		vNorms[0][2] = 0;
			vNorms[1][0] = 0;		vNorms[1][1] = 1;		vNorms[1][2] = 0;
			vNorms[2][0] = 0;		vNorms[2][1] = 1;		vNorms[2][2] = 0;
			vNorms[3][0] = 0;		vNorms[3][1] = 1;		vNorms[3][2] = 0;

			for(GLuint i =0; i< 4; ++i)
			{
				pVerts[this->iNoVerts] = vVerts[i];
				pNorms[this->iNoVerts] = vNorms[i];
				pTexCoords[this->iNoVerts] = vTexCoords[i];
				//pIndexes[iNoFaces] = iNoVerts;
				++iNoVerts;
			}
		}
	}
	EndMesh();
}


void Mesh::BeginMesh(GLuint nMaxVerts) {

	pVerts = new glm::vec3[nMaxVerts];
	pNorms = new glm::vec3[nMaxVerts];
	pIndexes = new GLuint[nMaxVerts];
	pTexCoords = new glm::vec2[nMaxVerts];

	iNoVerts = 0;
	iNoFaces = 0;
	iNoBuffers = 5;

	vbo = new GLuint[iNoBuffers];
	vao = 0;
}

void Mesh::AddTriangle(glm::vec3 verts[3], glm::vec3 norms[3], glm::vec2 texCoords[3]) {

	const float close = 0.0000001f;

	for(GLuint i =0; i< 4; ++i)
	{
		glm::normalize(norms[i]);
	}	
	GLuint j = 0;
	for(GLuint i =0; i< 3; ++i)
	{
		pVerts[this->iNoVerts] = verts[i];
		pNorms[this->iNoVerts] = norms[i];
		pTexCoords[this->iNoVerts] = texCoords[i];
		//pIndexes[iNoFaces] = iNoVerts;
		++iNoVerts;

		//this indexing isn't working
		//so just render each triangle for now

		/*for(j =0; j < iNoVerts; ++j)
		{
			if( comparePoints( verts[i], pVerts[j], close) &&
				comparePoints( norms[i], pNorms[j], close) &&
				comparePoints( texCoords[i], pTexCoords[j], close))
			{
				//this point is part of the mesh already, include only within the index list
				this->pIndexes[iNoFaces] = j;
				this->iNoFaces++;
				break;
			}
		}
		if(j == iNoVerts)
		{
				//this is a new point to be added to index list
				pVerts[this->iNoVerts] = verts[i];
				pNorms[this->iNoVerts] = norms[i];
				pTexCoords[this->iNoVerts] = texCoords[i];
				this->pIndexes[iNoFaces] = iNoVerts;
				++iNoVerts;
				++iNoFaces;
		}*/
	}


}

void Mesh::EndMesh() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::cout << "Hello end! " << std::endl;

	glGenBuffers(this->iNoBuffers, vbo);

	//bind first vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec3), pVerts , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);

	//bind normals buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec3), pNorms , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	//bind texturedata
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec2), pTexCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);   

	//bind color buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec3), pNorms , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);

	//indices to be bound next!
	//note the GL_ELEMENT_ARRAY_BUFFER is not GL_ARRAY_BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[4]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iNoFaces * sizeof(GLuint), pIndexes,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//clear up
	delete [] pIndexes;
    delete [] pVerts;
    delete [] pNorms;
    delete [] pTexCoords;

    // Reasign pointers so they are marked as unused
    pIndexes = NULL;
    pVerts = NULL;
    pNorms = NULL;
    pTexCoords = NULL;
}

//
// helper function to compare to points and return true if they are within margin to be considered the same
// much like m3d Close enough
// return true is same point
//

bool Mesh::comparePoints( glm::vec3 &p0, glm::vec3 &p1, GLfloat close ) {
	if(abs ( p0.x - p1.x) < close &&
		abs ( p0.y - p1.y) < close &&
		abs ( p0.z - p1.z) < close)
	{	return true;	}
	else
	{return false;}
}

bool Mesh::comparePoints( glm::vec2 &p0, glm::vec2 &p1, GLfloat close ) {
	if(abs ( p0.x - p1.x) < close &&
		abs ( p0.y - p1.y) < close)
	{	return true;	}
	else
	{return false;}
}

void Mesh::Draw(AGPShader* shader) {
	glBindVertexArray(this->vao);
	//move this to the object class
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX); 
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	/*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[4]);
	glDrawElements(GL_TRIANGLES, this->iNoFaces, GL_UNSIGNED_INT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);*/

	glDrawArrays(GL_TRIANGLE_FAN, 0, iNoVerts);

	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	glDisableVertexAttribArray(ATTRIBUTE_NORMAL);
	glDisableVertexAttribArray(ATTRIBUTE_TEXTURE0);
}

void Mesh::Update() {

}