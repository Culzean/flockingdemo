
//some constants
__constant int		MAX_BOIDS =		256;

#define				SEPERATION_DIST		0.4f
#define				MAX_VIEW			8

__constant float	MAX_SPEED =			0.1f;
__constant float	MIN_URGENCY =		0.0005f;
__constant float	MAX_URGENCY =		0.001f;
__constant float	VIEW_DIST =			4.9f;


typedef struct {
	float4 pos, vel, accel;
	int iBoidSeen;
} Boid __attribute((aligned(64)));

float4 addWander(float4 vel, float4 accel, float4 rand) {

	float4 change;
	change.xyzw = 0.0f;

	float diff = (fast_length(change) - (MAX_SPEED / 2)) / MAX_SPEED;
	float urgency = fabs(diff);

	if (urgency < MIN_URGENCY) urgency = MIN_URGENCY;
	if (urgency > MAX_URGENCY) urgency = MAX_URGENCY;

	int dir;
	float4 comp;
	comp.x = 0.5f;		comp.y = 0.45f;		comp.z = 0.92f;
	if(rand.y > comp.x)
	   dir = -1;
   else
	   dir = 1;

	if (rand.x < comp.y) {
      change.x += (urgency * dir);
   } else if (rand.x < comp.z) {
      change.z += (urgency * dir);
   } else {
      change.y += (urgency * dir);  // we don't like vertical motion all that much
   }

   return change; 
}

//take a boid that has found a near by neighbour
// and has stored the data relvant to course correction
//ie pos and vel

float4 avoidNear(float4 _boidPos, float4 _localPos , float _fNearestDist, float _fSepDist) {
	float4 change;
	change = 0.0f;

	float ratio = _fNearestDist / SEPERATION_DIST;

	if (ratio < MIN_URGENCY) ratio = MIN_URGENCY;
	if (ratio > MAX_URGENCY) ratio = MAX_URGENCY;

	if( _fNearestDist > _fSepDist)
	{	
		//too far apart
		change = normalize(_boidPos - _localPos);
		change *= ratio;
	}
	else if( _fNearestDist < _fSepDist)
	{
		//too near
		change = normalize(_boidPos - _localPos);
		change *= -ratio;
	}else {
		change.x = 0.0f;	change.y = 0.0f;	change.z = 0.0f;
	}

	return change;
}

__kernel
void boids(__global Boid* inBoid,
			float4 input,
			__global float4* rand,
            __global float4 *pos,
			const int iBoids,
			float3 bounds,
			float maxSpeed) {

	int idx = get_global_id(0);

	//who is around about us?

	int i,viewCount;
	Boid viewBoid[MAX_VIEW];
	Boid locNearest;
	float fNearestDist = 99999999999.0f;
	viewCount =0;

	inBoid[idx].pos = pos[idx];

	float dist;
	for(i=idx+1; i<iBoids; ++i)
	{
		dist = distance( inBoid[i].pos, inBoid[idx].pos );
		if( dist < fNearestDist )
		{
			//update nearest
			fNearestDist = dist;
			locNearest.pos = inBoid[i].pos;
			locNearest.vel = inBoid[i].vel;
		}
		
		if( dist < VIEW_DIST)
		{
			viewBoid[viewCount].pos = inBoid[i].pos;
			++viewCount;
			if(viewCount >= input.x)
				{
					i = iBoids;
				}
		}
	}
	inBoid[idx].iBoidSeen = viewCount;
	float3 posLoc = pos[idx].xyz;
	barrier(CLK_LOCAL_MEM_FENCE);

	//end check with flock

	inBoid[idx].pos.xyz += inBoid[idx].vel.xyz;
	inBoid[idx].vel.xyz += inBoid[idx].accel.xyz;

	//check bounds
	if(inBoid[idx].pos.x < 0)	{
		inBoid[idx].pos.x = bounds.x;	}
	else if(inBoid[idx].pos.x > bounds.x)	{
		inBoid[idx].pos.x = 0;	}
	if(inBoid[idx].pos.y < 0)	{
		inBoid[idx].pos.y = 1;
		inBoid[idx].vel.y *= -1;	}
	else if(inBoid[idx].pos.y > bounds.y)	{
		inBoid[idx].pos.y = bounds.y - 1;
		inBoid[idx].vel.y *= -1;	}
	if(inBoid[idx].pos.z < 0)	{
		inBoid[idx].pos.z = bounds.z;	}
	else if(inBoid[idx].pos.z > bounds.z)	{
		inBoid[idx].pos.z = 0;	}

   //restrain vel
  if(fabs(fast_length(inBoid[idx].vel)) > maxSpeed )
  {
		inBoid[idx].vel = fast_normalize(inBoid[idx].vel) * maxSpeed;
	}
	posLoc.xyz += inBoid[idx].vel.xyz;


	if(inBoid[idx].iBoidSeen > 0)
	{
		//check for group centre
		float4 centre;
		centre.xyzw = 0.0f;

		i =0;
		while(i < viewCount)
		{
			centre.xyz = viewBoid[i].pos.xyz;
			++i;
		}
		centre /= i;
		float4 toCentre = (centre - inBoid[idx].pos);
		float4 change = normalize(toCentre);
		change *= MIN_URGENCY;

		//move to centre
		//inBoid[idx].accel.xyz += change.xyz;
		//check how close nearest is
		inBoid[idx].accel.xyz += avoidNear(inBoid[idx].pos, locNearest.pos, fNearestDist, input.y).xyz;
		//match speed
		inBoid[idx].accel.xyz += ( (locNearest.vel - inBoid[idx].vel).xyz) * input.w;
		//inBoid[idx].vel = locNearest.vel;
		
	}

	//add 
	inBoid[idx].accel.xyz += addWander(inBoid[idx].vel, inBoid[idx].accel, rand[idx]).xyz;

	//output data
	pos[idx].xyz = inBoid[idx].pos.xyz;
}