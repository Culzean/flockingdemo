//texture vertex shader - texture.vert

uniform mat4x4 MV;
uniform mat4x4 projection;
uniform vec4 lightPosition;

attribute vec3 in_Position;
attribute vec3 in_Normal;
attribute vec2 in_TexCoord;
varying vec3 ex_N;
varying vec3 ex_V;
varying vec3 ex_L;
varying vec2 ex_TexCoord;



//multiply each vertex position by the MVP matrix
//and find V, L, N vectors for the fragment shader
void main(void)
{
	//vertex into eye coordinates
	vec4 vertexPosition = MV * vec4(in_Position, 1.0);
	gl_Position = projection * vertexPosition;

	//Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition.xyz);

	//Vertex normal in eye coordinates
	//if(in_Normal.x < 0 || in_Normal.y < 0 || in_Normal.z < 0)
	//	in_Normal = -in_Normal;
	ex_N = normalize(MV * vec4(in_Normal, 0.0)).xyz;

	//L- to light oruce from vertex
	ex_L = normalize(lightPosition.xyz - vertexPosition.xyz);

	//ex_TexCoord = in_Position.xy + vec2(0.5);

	ex_TexCoord = in_TexCoord;

}