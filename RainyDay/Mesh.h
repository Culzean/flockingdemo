#ifndef MESH_H
#define MESH_H

#include "stafx.h"
#include "Renderable.h"

/*
Much of this class is code handling a 3d mesh taken from last years project.
much a great deal of refitting has been carried out to get this up
to sdl glm standard and working with current shares plus the rendering heirachy

*/

class Mesh : public Renderable {

public:
	Mesh();
	virtual ~Mesh();

	virtual void Draw(AGPShader* shader);
	virtual void Update();
	virtual void Init();

	void createVertices(GLfloat iWidth, GLfloat iDepth, GLint iMaxWidth, GLint iMaxDepth);

	void BeginMesh(GLuint nMaxVerts);
	void EndMesh();
	void AddTriangle(glm::vec3 verts[3], glm::vec3 norms[3], glm::vec2 texCoords[3]);

private:

	bool loadData();

	bool comparePoints( glm::vec3 &p0, glm::vec3 &p1, GLfloat close );
	bool comparePoints( glm::vec2 &p0, glm::vec2 &p1, GLfloat close );

};

#endif