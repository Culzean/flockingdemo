#ifndef AGP_MATRIXSTACK_H
#define AGP_MATRIXSTACK_H

#include "stafx.h"
#include <stack>

class AGPMatrixStack{

private:
	std::stack<glm::mat4*> matrixStack;

	void clearStack();
	glm::mat4*	identity;

public:
	AGPMatrixStack();
	~AGPMatrixStack();

	void LoadIdentity();

	void pop();
	void push();
	void push(glm::mat4 crtMat);
	
	void multiMatrix(glm::mat4);
	void loadMatrix(glm::mat4 startMat);

	void scale(glm::vec3);
	void rotate(float rad, glm::vec3 dims);
	void rotate(float rad,float x,float y,float z);
	void translate(glm::vec3 dims);

	glm::mat4 getMatrix();
	void setProjection( glm::mat4 proj );
};

#endif